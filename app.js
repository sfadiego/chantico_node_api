require('./config/config');
require('./app/db/handleConnection')(1);
var express = require('express');
var path = require('path');
var logger = require('morgan');
var app = express();
var cors = require('cors');
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
// body parse
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//routes
app.use(require('./app/routes/index.js'));


//Routeo a chantico
if (process.env.NODE_ENV !== 'dev') {
    app.use(express.static(path.join(__dirname, 'build')));
    app.get('/*', (req, res) => {
        res.sendFile(path.join(__dirname, 'build', 'index.html'));
    });
}


module.exports = app;