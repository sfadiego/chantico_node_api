const DetalleProducto = require('../model/detalleproducto.model.js');
const GLOBAL = require('./globalconstantes');

// Create and Save a new detalleProducto
exports.create = (req, res) => {
    let validacion = validaciones(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }

    detalleProducto(req, res)
    .then(response => {
       res.send({ response  });
    }).catch(error => res.status(500).send({ message: error }) );
    
};

// Retrieve and return all usuarios from the database.
exports.findAll = (req, res) => {
    DetalleProducto.find()
    .then(callbackUsuairo => {
        res.send(callbackUsuairo);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single detalleProducto with a detalle_producto_id
exports.findOne = (req, res) => {
    let id = req.params.detalle_producto_id;    
    findOneElement(id)
    .then(response => {
        res.send(response);
    }).catch(err => {
        return res.status(400).send(err);
    });
};

// Update a detalleProducto identified by the detalle_producto_id in the request
exports.update = (req, res) => {
    // Validate Request
    let validacion = validacionesUpdate(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }

    let id = req.params.detalle_producto_id;
    actualizaDetalle(id, req.body)
    .then(response => {
        res.send({ response });
    }).catch(error => res.status(500).send({  message: error }));
};

// Delete a detalleProducto with the specified noteId in the request
exports.delete = (req, res) => {
    DetalleProducto.findByIdAndRemove(req.params.detalle_producto_id)
    .then(detalleProducto => {
        if(!detalleProducto) {
            return res.status(404).send({
                message: "DetalleProducto not found with id " + req.params.detalle_producto_id
            });
        }
        res.send({message: "DetalleProducto deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "DetalleProducto not found with id " + req.params.detalle_producto_id
            });                
        }
        return res.status(500).send({
            message: "Could not delete detalleProducto with id " + req.params.detalle_producto_id
        });
    });
};



// TODO: abstraer funciones
let detalleProducto = async (req)=>{
    let crearDetalle = await nuevoDetalle(req.body);
    return crearDetalle;
}



let validaciones =(req)=>{
    let data = {
        "message" : '',
        "status" : true
    };
    
    if (!req.body.producto_id) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.PRODUCTO_ID
        return data;
    }
    
    return data;
}


let validacionesUpdate = (req) => {
    let data = {
        "message": '',
        "status": true
    };

    if (!req.params.detalle_producto_id) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.DETALLE_PRODUCTO_ID
        return data;
    }

    return data;
}

let nuevoDetalle = (request)=>{
    return new Promise((resolve, reject)=>{
        const detalleProducto = new DetalleProducto({
            producto_id: request.producto_id,
            producto_abreviado: request.producto_abreviado || null,
            descripcion: request.descripcion || null,
            precio_proveedor: request.precio_proveedor || null,
            inventario: request.inventario || null,
            existencia: request.existencia || null,
            id_color: request.id_color || null,
            inventariado: request.inventariado || null,
            
        });

        detalleProducto.save()
        .then(response => {
            resolve(response);
        }).catch(errCallback => {
            reject(errCallback);
        });
    });
}


let actualizaDetalle = (idDetalle, request) => {
    return new Promise((resolve, reject) => {
        let id = idDetalle;
        DetalleProducto.findByIdAndUpdate({
            _id: id
        }, {
            producto_abreviado: request.producto_abreviado,
            descripcion: request.descripcion,
            precio_proveedor: request.precio_proveedor,
            inventario: request.inventario,
            existencia: request.existencia,
            id_color: request.id_color,
            inventariado: request.inventariado,
            
        })
        .then(response => {
            resolve(response);
        }).catch(errCallback => {
            reject(errCallback);
        });
        
    });
}

let findOneElement =(id)=>{
    return new Promise((resolve, reject)=>{
        DetalleProducto.findById({
            _id: id
        }).then(success => {
            if (!success) {
               return resolve({
                   message: GLOBAL.NOT_FOUND_ID,
                   parametro: id
               })
            }
            return resolve(success)
        }).catch(error => {
            if (error.kind === GLOBAL.ERROR_KIND.OBJECTID ) {
                return reject({
                    message: GLOBAL.NOT_WELL_FORMED_ID,
                    error:error.message
                })
            }

            return reject(error);

        });
    })
}