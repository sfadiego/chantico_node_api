const Proteina = require('../model/productos/proteina.model');
const File = require('../model/fileUpload.model');
const GLOBAL = require('./globalconstantes');

exports.crear = (request, response) =>{
    const proteinaObj = new Proteina.proteina({
        nombre: request.body.nombre,
        precio:request.body.precio
    });
    
    
    proteinaObj
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(500).send(error);
    });
}

 exports.crearrelacion = (request, response) =>{
    const productoproteinaObj = new Proteina.productoproteina({
        producto_id: request.body.producto_id,
        proteina_id: request.body.proteina_id,
    });

    productoproteinaObj
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(500).send(error);
    });
}

exports.getAllTipoproteina = (request, response) =>{
    getProteinasYArchivos()
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

let getProteinasYArchivos = async()=>{
    let proteinas = await getProteinas();
    let items_proteinas = proteinas.map(async(item) =>{
        let data = {};
        data._id = item._id;
        data.nombre = item.nombre;
        data.precio = item.precio;
        let file_info = await getFilesByItemId(item._id).then((data)=> {return data}).catch(error=>{return error});
        data.img = file_info;
        return data;    
    });
    
    let finalData = await Promise.all(items_proteinas);
    return finalData;
}

let getProteinas = ()=>{
    return new Promise((resolve,reject)=>{
        Proteina.proteina.find()
        .then(responseData => {
            resolve(responseData);
        }).catch(errorData => {
            reject(errorData)
        });
    });
}
let getFilesByItemId = (item_id) => {
    return new Promise((resolve, reject)=>{
        File.fileSchema.find({ item_id: item_id })
            .then(responseData => {
                resolve(responseData);
            }).catch(errorData => {
                reject(errorData);
            });
    });
}

exports.getrelacionproductosproteina = (request, response) =>{
    var id = request.params.producto_id;    
    Proteina.productoproteina.find({ producto_id: id  })
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.deleterelacionproductosproteina = (request, response)=>{
    let id_relacion = request.params.id_relacion;
    Proteina.productoproteina.findByIdAndRemove(id_relacion)
    .then((result) => {
        response.send(result);
    }).catch((err) => {
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING,
            error: err,
        });
    });
}

exports.update = (request, response) =>{
    if(!request.params.id) {
        return res.status(400).send({
            parametro:GLOBAL.ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }
    let id = request.params.id;
    Proteina.proteina.findByIdAndUpdate({
        _id: id
    }, {  
        nombre: request.body.nombre,
        precio: request.body.precio,
    })
    .then(responseData =>  response.send(responseData))
    .catch(errorData => {
        response.status(500).send({ message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA });
    });
};

exports.getOne = (request, response)=>{
    Proteina.proteina.findOne({_id: request.params.id })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}