const Usuarios = require('../model/usuarios.model');
const functions = require('../core/genericfunctions.controller');
const GLOBAL = require('../controllers/globalconstantes');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
exports.login = (req, response) => {
    let body = req.body;
    let username = body.username;
    let password = body.password;
    Usuarios.findOne({ username: username })
    .populate('tienda')
        .then(dataResponse => {
            if (!dataResponse) {
                let error = {
                    msg: `${GLOBAL.ERROR_CREDENTIALS}`,
                }
                return response.status(400).send(functions.errorHandler(error, 400));
            }
            if (!bcrypt.compareSync(password, dataResponse.password)) {
                let error = {
                    msg: `${GLOBAL.ERROR_CREDENTIALS}`,
                }
                return response.status(400).send(functions.errorHandler(error, 400));
            }
            let token = jwt.sign({
                usuario: dataResponse.username,
                nombre: dataResponse.nombre,
                apellido: dataResponse.apellido,
                role: dataResponse.role,
            }, 'secret-seed', { expiresIn: '8h' });

            return response.send({
                ok: true,
                data: dataResponse,
                token
            });
        })
        .catch(error => {
            response.status(500).send(functions.errorHandler(error, 500))
        })
}