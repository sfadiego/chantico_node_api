const Pedido = require('../model/pedidos.model');
const GLOBAL = require('../controllers/globalconstantes');
const GenericFunctions = require('../core/genericfunctions.controller');

const validaciones = (req) => {
    data = { "message": '', "status": true };

    if (!req.body.estatus_id) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ESTATUS_ID
        return data;
    }

    return data;
}

exports.create = (req, res) => {
    var validacion = validaciones(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }

    let data = {
        total: req.body.total || 0,
        subtotal: req.body.subtotal || 0,
        estatus_id: req.body.estatus_id,
        descuento: req.body.descuento || 0,
        nombre_pedido: req.body.nombre_pedido || GenericFunctions.generaFolio()
    };

    pedido(data)
        .then(success => res.send(success))
        .catch(error => res.status(500).send({ message: error.message || GLOBAL.ERROR_SAVING }));
};

let pedido = async (data) => await crearPedido(data);

let crearPedido = (data) => {
    return new Promise((resolve, reject) => {
        const pedido = new Pedido.pedido({
            total: data.total,

            descuento: data.descuento || 0,
            fecha_pedido: GenericFunctions.getDate(),
            nombre_pedido: data.nombre_pedido,
            subtotal: data.subtotal,
            estatus_id: data.estatus_id
        });
        pedido.save()
            .then(response => resolve(response))
            .catch(error => reject(error));
    })
}


exports.findAll = (req, res) => {
    Pedido.pedido.find()
        .populate('estatus_id')
        .then(response => res.send(response))
        .catch(err => res.status(500).send({ message: err.message || GLOBAL.ERROR_RETRIEVING_DATA }));
};

exports.getComandasActivas = (req, res) => {
    getEstatusOcupado()
        .then(response => res.send(response))
        .catch(err => res.status(500).send({ message: GLOBAL.UNAVAILABLE_CONTENT }));
};

let getEstatusOcupado = async () => {
    let getEstatusOcupado = await GenericFunctions.getStatusOcupado();
    let getEstatusProceso = await GenericFunctions.getEstatusProceso();
    if (getEstatusOcupado.length === 0 || !getEstatusProceso.length === 0) {
        return new Promise((resolve, reject) => {
            return reject({ message: GLOBAL.UNAVAILABLE_CONTENT })
        })
    }
    let estatus_id = {
        proceso_id: getEstatusProceso[0]._id,
        ocupado_id: getEstatusOcupado[0]._id
    };

    return new Promise((resolve, reject) => {
        Pedido.pedido
            .find({
                $and: [
                    { $or: [{ "estatus_id": estatus_id.ocupado_id }, { "estatus_id": estatus_id.proceso_id }] }
                ]
            })
            .populate('estatus_id')
            .then(response => resolve(response))
            .catch(err => reject(err))
    })
}


exports.findOne = (req, res) => {
    var id = req.params.pedidoId;
    Pedido.pedido.findById({ _id: id })
        .then(callbackResponse => {
            if (!callbackResponse) {
                return res.status(404).send({
                    message: GLOBAL.NOT_FOUND_ID
                });
            }
            res.send(callbackResponse);
        }).catch(err => {
            if (err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
                return res.status(404).send({
                    message: GLOBAL.NOT_WELL_FORMED_ID
                });
            }
            return res.status(500).send({
                message: GLOBAL.ERROR_RETRIEVING_DATA
            });
        });
};



// TODO:
exports.createpedidoItems = (req, res) => {
    let item = req.body.item;
    let precio = req.body.precio;
    let unique = req.body.unique;
    let key = req.body.key;

    let pedido_id = req.body.pedido_id;
    let producto_id = req.body.producto_id;
    let data_extra = {
        item,
        precio,
        unique,
        key
    }
    GenericFunctions
        .crear_relacion_pedido_item(pedido_id, data_extra, producto_id)
        .then(response => res.send(response))
        .catch(error => res.status(500).send({ message: error }));
}

exports.finalizapedido = (req, resp) => {
    let estatus_id = req.body.estatus_id;
    let id = req.params.pedido_id;
    Pedido.pedido.findByIdAndUpdate({ _id: id }, { estatus_id: estatus_id })
        .then(response => resp.send(response))
        .catch(error => resp.status(500).send({ message: error }));
}

exports.pedidoOcupado = (req, resp) => {
    if (!req.body.estatus_id) {
        return resp.status(400).send({
            message: GLOBAL.MISSING_PARAMETER,
            status: false,
            parametro: GLOBAL.PARAMETERS.ESTATUS_ID
        });
    }
    let estatus_id = req.body.estatus_id;
    let id = req.params.pedido_id;
    Pedido.pedido.findByIdAndUpdate({ _id: id }, { estatus_id: estatus_id })
        .then(response => resp.send(response))
        .catch(error => resp.status(500).send({ message: error }));
}


exports.pedidoFinalizados = (req, response) => {
    let fechas = req.body.fecha;
    getEstatusFinalizado(fechas)
        .then(success => response.send(success))
        .catch(errData => response.status(500).send({ message: GLOBAL.UNAVAILABLE_CONTENT }));
};

let getEstatusFinalizado = async (fecha = null) => {
    let estatus = await GenericFunctions.getEstatusFinalizado();
    let date = (fecha) ? new Date(fecha) : new Date();
    let month = ("0" + (date.getMonth() + 1)).slice(-2)
    let fecha_inicio = `${date.getFullYear()}-${month}-${date.getDate()}`;
    return new Promise((resolve, reject) => {
        Pedido.pedido.find({ createdAt: { $gte: fecha_inicio }, estatus_id: estatus[0]._id })
            .populate('estatus_id')
            .then(success => resolve(success))
            .catch(error => reject(error));
    });
}

exports.pedidoFinalizadosByFecha = (req, response) => {
    let fechas = req.body.fecha;
    getEstatusFinalizado(fechas)
        .then(success => response.send(success))
        .catch(errData => response.status(500).send({ message: errData.message || GLOBAL.ERROR_RETRIEVING_DATA }));
};


exports.totalVentas = (req, response) => {
    calculartotalVentaDiaria()
        .then(success => response.send(success))
        .catch((err) => resp.status(500).send(err))
}

let calculartotalVentaDiaria = async () => {
    let fecha = new Date();
    let month = ("0" + (fecha.getMonth() + 1)).slice(-2)
    let formatedDate = new Date(`${fecha.getFullYear()}-${month}-${fecha.getDate()}`);
    let resultado = await Pedido.pedido.find({ createdAt: { $gte: formatedDate } })
    let total = 0;
    resultado.map(item => {
        total = total + item.total
    })

    return { ventaTotal: total };
}