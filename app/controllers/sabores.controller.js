const SaboresModel = require('../model/productos/sabores.model');
const File = require('../model/fileUpload.model');
const GLOBAL = require('./globalconstantes');
const functions = require('../core/genericfunctions.controller');
exports.crear = (request, response) =>{
    const saboresObj = new SaboresModel.sabores({
        nombre: request.body.nombre,
        precio:request.body.precio
    });

    saboresObj
    .save()
    .then(responseData => response.send(responseData))
    .catch(error => response.status(500).send(error));
}

exports.getAllSabores = (request, response)=>{
    getProteinasYArchivos()
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

let getProteinasYArchivos = async()=>{
    let sabores = await getSabores();
    let items_sabores = sabores.map(async(item) =>{
        let data = {};
        data._id = item._id;
        data.nombre = item.nombre;
        data.precio = item.precio;
        let file_info = await getFilesByItemId(item._id).then((data)=> {return data}).catch(error=>{return error});
        data.img = file_info;
        return data;    
    });
    
    let finalData = await Promise.all(items_sabores);
    return finalData;
}

let getSabores = ()=>{
    return new Promise((resolve,reject)=>{
        SaboresModel.sabores.find()
        .then(responseData => {
            resolve(responseData);
        }).catch(errorData => {
            reject(errorData)
        });
    });
}
let getFilesByItemId = (item_id) => {
    return new Promise((resolve, reject)=>{
        File.fileSchema.find({ item_id: item_id })
            .then(responseData => {
                resolve(responseData);
            }).catch(errorData => {
                reject(errorData);
            });
    });
}

///sabores productos
exports.createrelproductosabores = (request, response) =>{
    const productoSaboresModel = new SaboresModel.productosabores({
        producto_id: request.body.producto_id,
        sabor_id: request.body.sabor_id
    });

    productoSaboresModel
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(400).send(error);
    });
}

exports.getrelacionproductossabores = (request, response)=>{
    var id = request.params.producto_id;    
    SaboresModel.productosabores.find({ producto_id: id  })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.deleterelacionproductos = (request, response)=>{
    let id_relacion = request.params.id_relacion;
    SaboresModel.productosabores.findByIdAndRemove(id_relacion)
    .then((result) => {
        response.send(result);
    }).catch((err) => {
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING,
            error: err,
        });
    });
}

exports.update = (request, response) =>{
    if(!request.params.id) {
        return res.status(400).send({
            parametro:GLOBAL.ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }
    let id = request.params.id;
    SaboresModel.sabores.findByIdAndUpdate({
        _id: id
    }, {  
        nombre: request.body.nombre,
        precio: request.body.precio,
    })
    .then(responseData =>  response.send(responseData))
    .catch(errorData => {
        response.status(500).send({ message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA });
    });
};

exports.getOne = (request, response)=>{
    SaboresModel.sabores.findOne({_id: request.params.id })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData =>response.status(500).send(functions.errorHandler(errorData)));
}

exports.getSaboresDulces = (request, response)=>{
    SaboresModel.sabordulce.find()
    .then(success => response.send(success))
    .catch(errorData=> response.status(500).send(functions.errorHandler(errorData)));
}

exports.createSaborDulce = (request, response)=>{
    const saboresObj = new SaboresModel.sabordulce({
        nombre: request.body.nombre,
        precio:request.body.precio
    });

    saboresObj
    .save()
    .then(responseData => response.send(responseData))
    .catch(error => response.status(500).send(functions.errorHandler(error)));
}

exports.updateSaborDulce = (request, response)=>{
    if(!request.params.id) {
        let error = {
            msg:`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.ID}`,
        }
        return response.status(400).send(functions.errorHandler(error,400));
    }

    let id = request.params.id;
    SaboresModel.sabordulce.findByIdAndUpdate({
        _id: id
    }, {  
        nombre: request.body.nombre,
        precio: request.body.precio,
    })
    .then(responseData =>  response.send(responseData))
    .catch(errorData =>response.status(500).send(functions.errorHandler(errorData,500)));
}

exports.getOneSaborDulce = (request, response)=>{
    SaboresModel.sabordulce.findOne({_id: request.params.id })
    .then(responseData => {
        if(!responseData) {  
            let error = {
                msg:`${GLOBAL.NOT_FOUND_ID}`,
            }
            return response.status(400).send(functions.errorHandler(error,400));
        }
        response.send(responseData);
    }).catch(errorData =>response.status(500).send(functions.errorHandler(errorData)));
}

exports.getrelacionproductossaboresdulces = (request, response)=>{
    var id = request.params.producto_id;    
    SaboresModel.productosaboresdulces.find({ producto_id: id  })
    .then(responseData => {
        if(!responseData) {  
            let error = {
                msg:`${GLOBAL.NOT_FOUND_ID}`,
            }
            return response.status(400).send(functions.errorHandler(error,400));
        }
        response.send(responseData);
    }).catch(errorData => response.status(500).send(functions.errorHandler(errorData,500)));
}

exports.deleterelacionproductosabordulce = (request, response)=>{
    let id_relacion = request.params.id_relacion;
    SaboresModel.productosaboresdulces.findByIdAndRemove(id_relacion)
    .then((result) => response.send(result))
    .catch((err) => response.status(500).send(functions.errorHandler(err,500)));
}

exports.createrelproductosaboresdulces = (request, response) =>{
    const productoSaboresModel = new SaboresModel.productosaboresdulces({
        producto_id: request.body.producto_id,
        sabor_id: request.body.sabor_id
    });

    productoSaboresModel
    .save()
    .then(responseData => response.send(responseData))
    .catch(error => response.status(500).send(functions.errorHandler(error,500)));
}
