const AderezosModel = require('../model/productos/aderezos.model');
const ProductoAderezosModel = require('../model/productos/productoaderezos.model');
const File = require('../model/fileUpload.model');
const GLOBAL = require('./globalconstantes');
exports.crear = (request, response) =>{
    const aderezoObj = new AderezosModel({
        nombre: request.body.nombre,
        precio:request.body.precio
    });

    aderezoObj
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(400).send(error);
    });
}

exports.getAll = (rq, response)=>{
    getAderezoYArchivos().then(success=>{
        response.send(success);
    }).catch(error=>{
        return response.status(500).send({  
            message: GLOBAL.ERROR_RETRIEVING_DATA, 
            error:error.message 
        });

    });
}

let getAderezoYArchivos = async()=>{
    let aderezos = await getAderezos();
    let items_aderezos = aderezos.map(async(item) =>{
        let data = {};
        data.nombre = item.nombre;
        data._id = item._id;
        data.precio = item.precio;
        let file_info = await getFilesByItemId(item._id).then((data)=> {return data}).catch(error=>{return error});
        data.img = file_info;
        return data;    
    });

    let finalData = await Promise.all(items_aderezos);
    return finalData;
}

let getAderezos = ()=>{
    return new Promise((resolve,reject)=>{
        AderezosModel.find()
        .then(responseData => {
            resolve(responseData);
        }).catch(errorData => {
            reject(errorData)
        });
    });
}
let getFilesByItemId = (item_id) => {
    return new Promise((resolve, reject)=>{
        File.fileSchema.find({ item_id: item_id })
            .then(responseData => {
                resolve(responseData);
            }).catch(errorData => {
                reject(errorData);
            });
    });
}

exports.getOne = (request, response)=>{
    if(!request.params.aderezo_id) {
        return res.status(400).send({
            parametro:GLOBAL.ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }
    AderezosModel.findOne({_id: request.params.aderezo_id })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID , data:aderezo_id }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA,
            data:aderezo_id
        });
    });
}

exports.update = (request, response) => {
    if (!request.params.aderezo_id) {
        return res.status(400).send({
            parametro:GLOBAL.PARAMETERS.ADEREZO_ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }
    
    var aderezo_id = request.params.aderezo_id;    
    AderezosModel.findByIdAndUpdate({ _id: aderezo_id }, {
        nombre: request.body.nombre,
        precio: request.body.precio,
    })
    .then(updateCallback => {
        if(!updateCallback) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID,
                error: updateCallback,
            });
        }
        return response.send(updateCallback);
    }).catch(errorResponse => {
        if(errorResponse.kind === GLOBAL.ERROR_KIND.OBJECTID) {
            return response.status(404).send({
                message: GLOBAL.NOT_FOUND_ID,
                error: errorResponse,
            });                
        }
        return response.status(500).send({
            message: GLOBAL.ERROR_UPDATING,
            error: errorResponse,
        });
    });
};


//Aderezo productos - relacion
exports.createrelproductoaderezos = (request, response) =>{
    const productoaderezoObj = new ProductoAderezosModel({
        producto_id: request.body.producto_id,
        aderezo_id: request.body.aderezo_id
    });

    productoaderezoObj
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(400).send(error);
    });
}

exports.getrelacionproductosaderezos = (request, response)=>{
    ProductoAderezosModel.find()
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.getOnerelation= (request, response)=>{
    var id = request.params.producto_id;    
    ProductoAderezosModel.find({ producto_id: id  })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.deleterelacionproductos = (request, response)=>{
    let id_relacion = request.params.id_relacion;
    ProductoAderezosModel.findByIdAndRemove(id_relacion)
    .then((result) => {
        response.send(result);
    }).catch((err) => {
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING,
            error: err,
        });
    });
}