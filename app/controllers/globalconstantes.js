const SUCCES_MESSAGE = "Se creo el registro exitosamente";
const SUCCES_DELETE = "Se borro el registro exitosamente";
const SUCCES = "Transacción Exitosa";
const SUCCESS_ORDER_UPDATED = "Orden actualizada"
const SUCCESS_ORDER_FINISHED = "Orden terminada"
const INVALID_FILE_TYPE = "Tipo de archivo no permitido";
const INVALID_ROLE = 'NO ES UN ROL VÁLIDO';

const MISSING_PARAMETER = "Campo requerido";
const FILE_REQUIRED = "El archivo es requerido";
const FILE_UPLOADED = "El archivo se subio correctamente";
const UPLOAD_PATH = "uploads/";
const PUBLIC_PATH = "imagen/";
const PUBLIC_NO_IMG_PATH = "noimagen/";
const FILE = 'archivo';
const FILE_DESTINATION_REQUIRED = "El destino del archivo no fue es requerido";
const ERROR = "Ha ocurrido un error";
const ERROR_SAVING = "Ha ocurrido un error creando registro";
const ERROR_UPDATING = "Ha ocurrido un error actualizando registro";
const ERROR_DELETING = "Ha ocurrido un error borrando registro";
const ERROR_RETRIEVING_DATA = "Error obteniendo información";
const UNAVAILABLE_CONTENT= "Contenido no disponible";
const ERROR_SEARCHING_DATA = "Error buscando información";
const ITEM = 'Asignar un Id a el registro';
const ITEM_ID = 'item_id';

const ERROR_UPDATING_RELATION = "Error actualizando relación";
const NOT_FOUND_ID = "No se encontro registro con ese id";
const NOT_VALID_ID = 'Id no valido';

// ERROR TYPES
const OBJECTID = 'ObjectId';
const NOT_FOUND = 'NotFound';
const NOT_WELL_FORMED_ID = 'No es un id formado correctamente';
const DUPLICATED_USERNAME = 'usuario duplicado'
const DUPLICATED_STORE = 'Tienda duplicada'
const ERROR_CREDENTIALS = 'Usuario o contraseña incorrectos'

//PARAMETROS
const CATEGORIA_ID = "categoria_id";
const ARCHIVO_ID = "archivo_id";
const CATEGORIA = "categoria";
const CANTIDAD = "cantidad";
const PRODUCTO = "producto";
const PRECIO = "precio";
const PRODUCTO_ID = "producto_id";
const ESTATUS_ID = "estatus_id";
const ESTATUS = "estatus";
const COLOR = "color";
const ICON = "icon";
const CSSCLASS = "cssclass";
const UNIQUE = "unique";
const PEDIDO_ID = "pedido_id";
const TOTAL = "total";
const SUBTOTAL = 'subtotal';
const NOMBRE_PRODUCTO = "nombre_producto";
const PRODUCTO_ABREVIADO = "producto_abreviado";
const PRECIO_PROVEEDOR = "precio_proveedor";
const DESCRIPCION = "descripcion";
const PRECIO_VENTA = "precio_venta";
const INVENTARIO = "inventario";
const EXISTENCIA = "existencia";
const ID_COLOR = "id_color";
const INVENTARIADO = "inventariado";
const URL_IMAGEN = "url_imagen";
const DETALLE_PRODUCTO_ID = "detalle_producto_id"
const FOLDER_NAME = 'nombre_carpeta';
const ADEREZO = 'aderezos';
const ADEREZO_ID = 'aderezo_id';
const FILE_PATH = 'file_path';
const FOLDER = 'folder'
const FILE_NAME = 'file_name'
const KEY = 'KEY'
const ID = 'Id'
const PASSWORD = 'password'
const USERNAME = 'username'

const NOMBRE = 'nombre';
const APELLIDO = 'apellido';
const TELEFONO = 'telefono';

const TIENDA_NOMBRE = 'Nombre de tienda';
const INVALID_TOKEN = 'INVALID_TOKEN';
const SABOR = 'sabor';
const SABOR_ID = 'sabor_id';
const TIPO_BEBIDA = 'tipo_bebida'
const TIPO_BEBIDA_ID = 'tipo_bebida_id'
const PROTEINA_ID = 'proteina_id'
const PROTEINA = 'proteina'
//models 

//Tablas Extras
const BEBIDAS = 1;

const ADMIN_ROLE = 1;
const USER_ROLE = 2;

var GLOBAL = {
    ID,
    ADEREZO,
    ADMIN_ROLE,
    USER_ROLE,
    SUCCESS_ORDER_FINISHED,
    SUCCESS_ORDER_UPDATED,
    SABOR,
    INVALID_TOKEN,
    UPLOAD_PATH,
    PUBLIC_PATH,
    PUBLIC_NO_IMG_PATH,
    FILE_REQUIRED,
    INVALID_FILE_TYPE,
    INVALID_ROLE,
    FILE_UPLOADED,
    SUCCES_MESSAGE,
    SUCCES: SUCCES,
    MISSING_PARAMETER,
    NOT_FOUND_ID,
    NOT_VALID_ID,
    ERROR_CREDENTIALS,
    ERROR,
    DUPLICATED_USERNAME,
    DUPLICATED_STORE,
    PASSWORD,
    USERNAME,
    ERROR_CREDENTIALS,
    ERROR_SAVING,
    ERROR_RETRIEVING_DATA,
    UNAVAILABLE_CONTENT,
    ERROR_SEARCHING_DATA,
    ERROR_UPDATING,
    ERROR_DELETING,
    SUCCES_DELETE,
    NOT_WELL_FORMED_ID,
    ERROR_UPDATING_RELATION,
    ITEM,
    EXTRAS: {
        BEBIDAS
    },
    PARAMETERS: {
        ITEM_ID,
        SUBTOTAL,
        NOMBRE,
        TIENDA_NOMBRE,
        APELLIDO,
        TELEFONO,
        ARCHIVO_ID,
        PROTEINA_ID,
        PROTEINA,
        TIPO_BEBIDA,
        TIPO_BEBIDA_ID,
        SABOR_ID,
        FILE,
        FOLDER,
        FILE_NAME,
        FOLDER_NAME,
        FILE_DESTINATION_REQUIRED,
        DETALLE_PRODUCTO_ID,
        CATEGORIA_ID,
        CANTIDAD,
        PRECIO: PRECIO,
        PRODUCTO: PRODUCTO,
        PRODUCTO_ID: PRODUCTO_ID,
        CATEGORIA: CATEGORIA,
        ESTATUS_ID: ESTATUS_ID,
        ESTATUS: ESTATUS,
        ICON: ICON,
        CSSCLASS: CSSCLASS,
        COLOR: COLOR,
        PEDIDO_ID: PEDIDO_ID,
        UNIQUE: UNIQUE,
        TOTAL: TOTAL,
        NOMBRE_PRODUCTO: NOMBRE_PRODUCTO,
        PRODUCTO_ABREVIADO: PRODUCTO_ABREVIADO,
        PRECIO_PROVEEDOR: PRECIO_PROVEEDOR,
        DESCRIPCION: DESCRIPCION,
        PRECIO_VENTA: PRECIO_VENTA,
        INVENTARIO: INVENTARIO,
        EXISTENCIA: EXISTENCIA,
        ID_COLOR: ID_COLOR,
        INVENTARIADO: INVENTARIADO,
        URL_IMAGEN: URL_IMAGEN,
        ADEREZO_ID,
        KEY,
        FILE_PATH,
    },
    ERROR_KIND: {
        OBJECTID: OBJECTID,
        NOT_FOUND: NOT_FOUND
    }
}
module.exports = GLOBAL;