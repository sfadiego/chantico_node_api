const GLOBAL = require('./globalconstantes');
const fileUploadModel = require('../model/fileUpload.model.js');


let validaFileUpload = (request) => {
    let data = {
        "message": '',
        "status": true,
        "parametro":null,
    };

    if (!request.params.nombre_carpeta) {
         data.message = GLOBAL.FILE_DESTINATION_REQUIRED
         data.status = false;
         data.parametro = GLOBAL.PARAMETERS.FOLDER_NAME
         return data;
    }

    if (!request.body.item_id) {
        data.message = GLOBAL.ITEM
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ITEM_ID
        return data;
    }

    if (!request.files && request.files === null) {
        data.message = GLOBAL.FILE_REQUIRED;
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.FILE_REQUIRED
        return data;
    }

     let extensionesValidas = ['jpg', 'JPG', 'JPEG', 'jpeg', 'PNG','png'];
     let nombre_split = request.files.archivo.name.split('.');
     let extension = nombre_split[nombre_split.length - 1];

    if (extensionesValidas.indexOf(extension) < 0) {
        data.message = GLOBAL.INVALID_FILE_TYPE + `, las extension [.${extension}] no esta permitida.`
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.FILE
        return data;
    }

    return data;
}

exports.uploadFile = (req, res) => {
    let validacion = validaFileUpload(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }
    
    handleUpload(req).then(success => {
        res.send(success);
    }).catch(error => {
        return res.status(400).send({
            message: GLOBAL.MISSING_PARAMETER,
            error: error.message,
        });
    });
}

let handleUpload = async (req) =>{
    let obj = getFileInfo(req);
    let nombre_archivo = obj.nombreArchivo.replace(/\s/g, '_');
    let registro = await registrarArchivo(obj.file_path, obj.carpetaArchivo, nombre_archivo, obj.public_path,req.body.item_id);
    // let relacion = await registrarRelacionArchivo(req.body.item_id, registro._id);
    let fileData = await uplaodFile(req).then(success=> success).catch(err => err);
    return {
      registro, 
      fileData
    };
}

let uplaodFile = (req) => {
    return new Promise((resolve, reject) => {
        let fileInfo = getFileInfo(req);
        let archivo = req.files.archivo;
        let file_path = fileInfo.file_path;
        archivo.mv(file_path, function (err) {
            if (err) { reject(err);  }
            resolve(GLOBAL.FILE_UPLOADED);
        });
    });
}

let getFileInfo = (req)=>{
    let archivo = req.files.archivo;
    let fecha = new Date();
    let nombreArchivo = `${fecha.getDay()}${fecha.getMonth()}${fecha.getFullYear()}_${archivo.name}`;
    let carpetaArchivo = req.params.nombre_carpeta;
    let file_path = GLOBAL.UPLOAD_PATH + `${carpetaArchivo}/${nombreArchivo}`;
    let public_path = GLOBAL.PUBLIC_PATH + `${carpetaArchivo}/${nombreArchivo}`;

    return {
        nombreArchivo,
        carpetaArchivo,
        file_path,
        public_path
    };
}

//TODO: Relacion producto 
let registrarArchivo = (file_path, foder_name, file_Name, publicpath, item_id) => {
    return new Promise((resolve, reject) => {
        const registro = new fileUploadModel.fileSchema({
            file_path: file_path,
            folder: foder_name,
            fileName: file_Name,
            public_path: publicpath,
            item_id:item_id
        });

        registro.save()
        .then(response => {
            resolve(response);
        }).catch(errCallback => {
            reject(errCallback);
        });
    })
}


exports.getFileByItemId = async (request, response)=>{
    var item_id = request.params.item_id;    
    fileUploadModel.fileSchema.find({ item_id: item_id  })
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message:GLOBAL.ERROR_RETRIEVING_DATA,
            data:errorData.message
        });
    });
}

