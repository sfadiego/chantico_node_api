const Usuarios = require('../model/usuarios.model.js');
const Tienda = require('../model/tienda.model');
const bcrypt = require('bcrypt');
const functions = require('../core/genericfunctions.controller');
const GLOBAL = require('./globalconstantes');

// Create and Save a new usuario
exports.create = (req, res) => {
    let body = req.body;

    if (!body.nombre) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.NOMBRE}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }

    if (!body.username) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.USERNAME}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }

    if (!body.password) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PASSWORD}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }

    if (!body.tienda) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIENDA_NOMBRE}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }
    guardausuarioYtienda(body)
        .then(callbackData => res.send({ data: callbackData, ok: true }))
        .catch(error => res.status(500).send(functions.errorHandler(error, 500)));
};

let guardausuarioYtienda = async (requestObject) => {
    const nombreTienda = requestObject.tienda;
    let existe = await existeTienda(nombreTienda);
    if (!existe) {
        let tienda = await guardarTienda(nombreTienda);
        let id = tienda._id
        let usuario = new Usuarios({
            nombre: requestObject.nombre,
            apellido: requestObject.apellido || "",
            username: requestObject.username,
            password: bcrypt.hashSync(requestObject.password, 10),
            telefono: requestObject.telefono,
            role: requestObject.role,
            tienda: id
        });
        return await guardarUsuario(usuario)
    } else {
        let tienda = await Tienda.findOne({ tienda: nombreTienda });
        let id = tienda._id;
        let usuario = new Usuarios({
            nombre: requestObject.nombre,
            apellido: requestObject.apellido || "",
            username: requestObject.username,
            password: bcrypt.hashSync(requestObject.password, 10),
            telefono: requestObject.telefono,
            role: requestObject.role,
            tienda: id
        });
        return await guardarUsuario(usuario)
    }
}

let existeTienda = (tienda) => {
    return new Promise((resolve, reject) => {
        Tienda.findOne({ tienda: tienda })
            .then(callbackData => resolve(callbackData))
            .catch(error => reject(error));
    });
}

let guardarTienda = (tienda) => {
    return new Promise((resolve, reject) => {
        let tiendaIntance = new Tienda({
            tienda: tienda,
            cerradaHasta: functions.getOpenSalesDate()
        })
        tiendaIntance.save()
            .then(callbackData => resolve(callbackData))
            .catch(error => reject(error));
    });
}

let guardarUsuario = (userInstance) => {
    return new Promise((resolve, reject) => {
        userInstance.save()
            .then(callbackData => resolve(callbackData))
            .catch(error => reject(error));
    });
}

// Retrieve and return all usuarios from the database.
exports.findAll = (req, res) => {
    Usuarios.find()
        .then(callbackUsuairo => res.send(callbackUsuairo))
        .catch(error => res.status(500).send(res.status(500).send(functions.errorHandler(error, 500))));
};

// Find a single usuario with a usuarioId
exports.findOne = (req, res) => {
    var id = req.params.usuarioId;
    Usuarios.findById({ _id: id })
        .then(callbackResponse => {
            if (!callbackResponse) {
                return res.status(404).send({
                    message: "Usuario no encontrado con ese id: " + id
                });
            }
            res.send(callbackResponse);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Usuario no encontrado con id: " + id
                });
            }
            return res.status(500).send({
                message: "Error buscando usuario con id: " + id
            });
        });
};

// Update a usuario identified by the usuarioId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.nombre) {
        return res.status(400).send({
            message: "Nombre de usuario No debe ser vacio."
        });
    }

    if (!req.body.apellido) {
        return res.status(400).send({
            message: "apellido No debe ser vacio."
        });
    }

    var id = req.params.usuarioId;
    // Find note and update it with the request body
    Usuarios.findByIdAndUpdate({ _id: id }, {
        nombre: req.body.nombre || "",
        apellido: req.body.apellido || "",
        username: req.body.username || "",
        password: req.body.password || "",
        role: req.body.role,
        telefono: req.body.telefono || ""
    }, { new: true })
        .then(updateCallback => {
            if (!updateCallback) {
                return res.status(404).send({
                    message: "Usuarios not found with id " + req.params.usuarioId
                });
            }
            res.send(updateCallback);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Usuarios not found with id " + req.params.usuarioId
                });
            }
            return res.status(500).send({
                message: "Error updating note with id " + req.params.usuarioId
            });
        });
};

// Delete a usuario with the specified noteId in the request
exports.delete = (req, res) => {
    Usuarios.findByIdAndRemove(req.params.usuarioId)
        .then(usuario => {
            if (!usuario) {
                return res.status(404).send({
                    message: "Usuarios not found with id " + req.params.usuarioId
                });
            }
            res.send({ message: "Usuarios deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Usuarios not found with id " + req.params.usuarioId
                });
            }
            return res.status(500).send({
                message: "Could not delete usuario with id " + req.params.usuarioId
            });
        });
};


let handleCerrarCaja = async (nombreTienda) => {
    let tienda = await Tienda.findOne({ tienda: nombreTienda });
    let id = tienda._id
    return await Tienda.updateOne({ _id: id }, { $set: { cajaCerrada: true, cerradaHasta: functions.getOpenSalesDate() } })
}

exports.cerrarCaja = (req, res) => {
    let body = req.body;
    let tienda = body.tienda;


    if (!body.tienda) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIENDA_NOMBRE}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }
    handleCerrarCaja(tienda)
        .then(callbackData => res.send({ data: callbackData, ok: true }))
        .catch(error => res.status(500).send(functions.errorHandler(error, 500)));
}


exports.abrirCaja = (req, res) => {
    let body = req.body;
    let tienda = body.tienda

    if (!body.tienda) {
        let error = {
            msg: `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIENDA_NOMBRE}`,
        }
        return res.status(400).send(functions.errorHandler(error, 400));
    }
    handleabrirCaja(tienda)
        .then(callbackData => res.send({ data: callbackData, ok: true }))
        .catch(error => res.status(500).send(functions.errorHandler(error, 500)));
}

let handleabrirCaja = async(nombreTienda) => {
    let tienda = await Tienda.findOne({ tienda: nombreTienda });
    let id = tienda._id
    return await Tienda.updateOne({ _id: id }, { $set: { cajaCerrada: false } })
}