const EstatusPedido = require('../model/catestatuspedido.model.js');
const GLOBAL = require('./globalconstantes');

const validaciones = (req) => {
    data = {
        "message": '', "status": true
    };

    if (!req.body.estatus) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ESTATUS
        return data;
    }

    if (!req.body.icon) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ICON
        return data;
    }

    if (!req.body.color) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.COLOR
        return data;
    }

    if (!req.body.cssclass) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.CSSCLASS
        return data;
    }

    return data;
}

const validacionesUpdate = (req) => {

    data = { "message": '', "status": true };

    if (!req.params.estatusId) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ESTATUS_ID
        return data;
    }

    if (!req.body.icon) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.ICON
        return data;
    }

    return data;
}

exports.create = (req, res) => {
    var validacion = validaciones(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }

    const estatusProducto = new EstatusPedido({
        estatus: req.body.estatus,
        icon: req.body.icon,
        color: req.body.color,
        cssclass: req.body.cssclass,
    });

    estatusProducto.save()
        .then(callbackData => {
            res.send({
                message: GLOBAL.SUCCES_MESSAGE,
                data: callbackData
            });
        }).catch(errCallback => {
            res.status(500).send({
                message: errCallback.message || GLOBAL.ERROR_SAVING
            });
        });
};

exports.findAll = (req, res) => {
    EstatusPedido.find()
        .then(callbackPedido =>  res.send(callbackPedido))
        .catch(err => {
            res.status(500).send({
                message: err.message || GLOBAL.ERROR_RETRIEVING_DATA
            });
        });
};

exports.findOne = (req, res) => {
    var id = req.params.estatusId;
    EstatusPedido.findById({ _id: id })
        .then(callbackResponse => {
            if (!callbackResponse) {
                return res.status(404).send({
                    message: GLOBAL.NOT_FOUND_ID
                });
            }
            res.send(callbackResponse);
        }).catch(err => {
            if (err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
                return res.status(404).send({
                    message: GLOBAL.NOT_WELL_FORMED_ID
                });
            }
            return res.status(500).send({
                message: GLOBAL.NOT_FOUND_ID
            });
        });
};

exports.update = (req, res) => {
    var validateUpdate = validacionesUpdate(req);
    if (!validateUpdate.status) {
        return res.status(400).send(validateUpdate);
    }

    var id = req.params.estatusId;
    EstatusPedido.findByIdAndUpdate({ _id: id }, {
        estatus: req.body.estatus,
        icon: req.body.icon,
        color: req.body.color,
        cssclass: req.body.cssclass,
    }, { new: true })
        .then(updateCallback => {
            if (!updateCallback) {
                return res.status(404).send({
                    message: GLOBAL.NOT_FOUND_ID
                });
            }
            res.send(updateCallback);
        }).catch(err => {
            if (err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
                return res.status(404).send({
                    message: GLOBAL.NOT_WELL_FORMED_ID
                });
            }
            return res.status(500).send({
                message: GLOBAL.ERROR_UPDATING
            });
        });
};

exports.delete = (req, res) => {
    EstatusPedido.findByIdAndRemove(req.params.estatusId)
        .then(estatusProducto => {
            if (!estatusProducto) {
                return res.status(404).send({
                    message: GLOBAL.NOT_FOUND_ID
                });
            }
            res.send({
                message: GLOBAL.SUCCES_DELETE
            });
        }).catch(err => {
            if (err.kind === GLOBAL.ERROR_KIND.OBJECTID || err.name === GLOBAL.ERROR_KIND.NOT_FOUND) {
                return res.status(404).send({
                    message: GLOBAL.NOT_FOUND_ID
                });
            }
            return res.status(500).send({
                message: GLOBAL.ERROR_DELETING
            });
        });
};