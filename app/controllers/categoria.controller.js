const GLOBAL = require('./globalconstantes');
const Categoria = require('../model/catcategorias.model.js');

exports.create = (req, res) => {
    if(!req.body.categoria) {
        return res.status(400).send({
            parametro:GLOBAL.PARAMETERS.CATEGORIA,
            message: GLOBAL.MISSING_PARAMETER
        });
    }

    const categoria = new Categoria({
        categoria: req.body.categoria,
        activo: req.body.activo || true
    });

    categoria.save()
    .then(callbackData => {
        res.send({data:callbackData, message: GLOBAL.SUCCES_MESSAGE});
    }).catch(errCallback => {
        res.status(500).send({
            message: errCallback.message || GLOBAL.ERROR_SAVING
        });
    });
};

exports.findAll = (req, res) => {
    Categoria.find()
    .then(response => {
        res.send(response);
    }).catch(err => {
        res.status(500).send({
            message: err.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
};

exports.findOne = (req, res) => {
    var id = req.params.categoriaId;    
    Categoria.findById({_id: id })
    .then(callbackResponse => {
        if(!callbackResponse) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });            
        }
        res.send(callbackResponse);
    }).catch(err => {
        if(err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_SEARCHING_DATA
        });
    });
};

exports.update = (req, res) => {
    if (!req.params.categoriaId) {
        return res.status(400).send({
            parametro:GLOBAL.PARAMETERS.CATEGORIA_ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }

    var id = req.params.categoriaId;    
    Categoria.findByIdAndUpdate({ _id: id }, {
        categoria: req.body.categoria || "",
        activo: req.body.activo || true
    }, {new: true})
    .then(updateCallback => {
        if(!updateCallback) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID,
                error: updateCallback,
            });
        }
        res.send(updateCallback);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID,
                error: err,
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_UPDATING,
            error: err,
        });
    });
};

exports.delete = (req, res) => {
    Categoria.findByIdAndRemove(req.params.categoriaId)
    .then(usuario => {
        if(!usuario) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });
        }
        res.send({message: GLOBAL.SUCCES_DELETE });
    }).catch(err => {
        if(err.kind === GLOBAL.ERROR_KIND.OBJECTID || err.name === GLOBAL.ERROR_KIND.NOT_FOUND) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING
        });
    });
};