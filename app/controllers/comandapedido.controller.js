const Comandapedido = require('../model/comandapedido.model.js');
const GLOBAL = require('./globalconstantes');
const CRUDS = require('../core/genericfunctions.controller');
const Pedidos = require('../model/pedidos.model');

exports.createComanda = (request, response) => {
    let data_comanda = request.body.data;
    let objeto_pedido = request.body.pedido;
    procesaRelacionComandaPedido(data_comanda, objeto_pedido)
        .then((result) => response.send(result))
        .catch((err) => response.status(500).send(err))
};

exports.getComandaByPedidoId = (request, response) => {
    let pedidoId = request.params.pedidoId;
    comandaYPedidoByPedidoId(pedidoId)
        .then((result) => response.send({ data: result, message: GLOBAL.SUCCESS_ORDER_UPDATED }))
        .catch((error) => response.status(500).send({ message: GLOBAL.ERROR_SAVING, data: error }));
}

let procesaRelacionComandaPedido = async (data_comanda, objeto_pedido) => {
    let data = data_comanda;
    await borrarExtras(objeto_pedido.id);
    await getExtrasComanda(data, objeto_pedido);
    await CRUDS.findAndUpdatePedido(objeto_pedido.id, objeto_pedido);
    await Comandapedido.deleteMany({ 'pedido_id': { '$in': objeto_pedido.id } });

    return await Promise.all(
        data.map(async item => { await CRUDS.save_relacion_comanda_pedido(item, objeto_pedido.id) })
    );
}

let borrarExtras = async (id) => {
    return await Pedidos.pedidoextras.deleteMany({
        'pedido_id': { '$in': id }
    }, (error) => error);
}

exports.getExtrasComandaByPedidoId = (req, response) => {
    let id = req.params.id;
    Pedidos.pedidoextras.find({ pedido_id: id })
        .then((result) => response.send(result))
        .catch((err) => response.status(500).send(err))
}

let getExtrasComanda = async (data, objeto_pedido) => {
    let id_pedido = objeto_pedido.id;
    data.map(async item => item.hasOwnProperty('extras') ? await guardarExtra(item, id_pedido) : null);
    return data;
}

let guardarExtra = async (obj, pedido_id) => {
    let producto = obj;
    let extra = producto.extras;
    let unique = producto.unique;
    let key = producto.key;

    return await Promise.all(
        extra.map(async extra => {
            let obj_extra = {
                key,
                unique,
                precio: extra.precio,
                nombre: extra.nombre
            }
            await CRUDS.crear_relacion_pedido_item(pedido_id, obj_extra)
        })
    );
}

let comandaYPedidoByPedidoId = async (pedido_id) => {
    let pedido = await CRUDS.findOnePedido(pedido_id);
    let comandapedido = await CRUDS.getRelComandaByPedidoId(pedido._id);
    return {
        pedido,
        comanda: comandapedido
    }
}

exports.getVentasDeDia = (req, resp) => {
    calcularVentas()
        .then(success => resp.send(success))
        .catch((err) => resp.status(500).send(err))
}

let calcularVentas = async () => {
    let fecha = new Date()
    let month = ("0" + (fecha.getMonth() + 1)).slice(-2)
    let formatedDate = new Date(`${fecha.getFullYear()}-${month}-${fecha.getDate()}`)
    let totalVenta = await Comandapedido.find({
        createdAt: { $gte:formatedDate }
    });
    let arrayFinal = [];
    totalVenta.map(async item => {
        if (!arrayFinal.find(filtrado => filtrado.key === item.key)) {
            const { key, producto, cantidad } = item;
            arrayFinal.push({ key, producto, cantidad })
        } else if (arrayFinal.find(filtrado => filtrado.key === item.key)) {
            arrayFinal.find(filtrado => filtrado.key === item.key ? filtrado.cantidad = filtrado.cantidad + item.cantidad : false)
        }
    })
    return arrayFinal
}