const TipoBebidaModel = require('../model/productos/tipobebida.model');
const File = require('../model/fileUpload.model');
const GLOBAL = require('./globalconstantes');

exports.crear = (request, response) =>{
    const tipoBebidaObj = new TipoBebidaModel.tipobebida({
        nombre: request.body.nombre,
        precio:request.body.precio
    });

    tipoBebidaObj
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(500).send(error);
    });
}

exports.getAllTipoBebida = (request, response)=>{
    tipoBebidasYArchivos()
    .then(responseData => {
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

let tipoBebidasYArchivos = async()=>{
    let tipobebida = await getTipoBebida();
    let items_bebida = tipobebida.map(async(item) =>{
        let data = {};
        data.nombre = item.nombre;
        data._id = item._id;
        data.precio = item.precio;
        let file_info = await getFilesByItemId(item._id).then((data)=> {return data}).catch(error=>{return error});
        data.img = file_info;
        return data;    
    });

    let finalData = await Promise.all(items_bebida);
    return finalData;
}

let getTipoBebida = ()=>{
    return new Promise((resolve,reject)=>{
        TipoBebidaModel.tipobebida.find()
        .then(responseData => {
            resolve(responseData);
        }).catch(errorData => {
            reject(errorData)
        });
    });
}

let getFilesByItemId = (item_id) => {
    return new Promise((resolve, reject)=>{
        File.fileSchema.find({ item_id: item_id })
            .then(responseData => {
                resolve(responseData);
            }).catch(errorData => {
                reject(errorData);
            });
    });
}

///sabores productos
exports.createrelproductotipobebida = (request, response) =>{
    const productoSaboresModel = new TipoBebidaModel.productotipobebida({
        producto_id: request.body.producto_id,
        tipo_bebida_id: request.body.tipo_bebida_id
    });

    productoSaboresModel
    .save()
    .then(responseData => {
        response.send(responseData);
    }).catch(error => {
        response.status(400).send(error);
    });
}

exports.getrelacionproductostipobebida = (request, response)=>{
    var id = request.params.producto_id;    
    TipoBebidaModel.productotipobebida.find({ producto_id: id  })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.deleterelacionproductostipobebida = (request, response)=>{
    let id_relacion = request.params.id_relacion;
    TipoBebidaModel.productotipobebida.findByIdAndRemove(id_relacion)
    .then((result) => {
        response.send(result);
    }).catch((err) => {
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING,
            error: err,
        });
    });
}

exports.relpedidoBebida = (request, response)=>{
    let pedido = request.params.pedido_id;
    TipoBebidaModel.pedidotipobebida.find({ pedido_id: pedido  })
    .then(responseData => response.send(responseData))
    .catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}

exports.createrelpedidoBebida = (request, response)=>{
    const pedidotipobebidaobject = new TipoBebidaModel.pedidotipobebida({
        pedido_id: request.body.pedido_id,
        tipo_bebida_id: request.body.tipo_bebida_id
    });

    pedidotipobebidaobject
    .save()
    .then(responseData => response.send(responseData))
    .catch(error => response.status(400).send(error));
}

exports.update = (request, response) =>{
    if(!request.params.id) {
        return res.status(400).send({
            parametro:GLOBAL.ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }
    let id = request.params.id;
    return new Promise((resolve, reject)=>{
        TipoBebidaModel.tipobebida.findByIdAndUpdate({
            _id: id
        }, {  
            nombre: request.body.nombre,
            precio: request.body.precio,
        })
        .then(responseData =>  response.send(responseData))
        .catch(errorData => {
            response.status(500).send({ message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA });
        });
    })
};

exports.getOne = (request, response)=>{
    TipoBebidaModel.tipobebida.findOne({_id: request.params.id })
    .then(responseData => {
        if(!responseData) {  return response.status(404).send({ message: GLOBAL.NOT_FOUND_ID }); }
        response.send(responseData);
    }).catch(errorData => {
        response.status(500).send({
            message: errorData.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
}