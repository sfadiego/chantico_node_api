const CatProductos = require('../model/catproductos.model.js');
const File = require('../model/fileUpload.model');
const globalFunctions = require('../core/genericfunctions.controller');
const mongoose = require('mongoose');
const GLOBAL = require('./globalconstantes');

let validaproductos = (request) => {
    let data = {
        "message": '',
        "status": true,
        "parametro":null,
    };
    
    if(!request.body.producto) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.PRODUCTO
        return data;
    }

    if (!request.body.precio) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.PRECIO
        return data;
    }

    if (!request.body.cantidad) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.CANTIDAD
        return data;
    }

    if (!request.body.categoria_id) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.status = false;
        data.parametro = GLOBAL.PARAMETERS.CATEGORIA_ID
        return data;
    }
    
    return data;
}

exports.create = (req, res) => {
    let validacion = validaproductos(req);
    if (!validacion.status) {
        return res.status(400).send(validacion);
    }
    
    const catproducto = new CatProductos({
        producto: req.body.producto,
        precio: req.body.precio,
        cantidad: req.body.cantidad,
        categoria_id: req.body.categoria_id,
        activo: req.body.activo || true
    });

    catproducto.save()
    .then(callbackData => {
        res.send({
            message: GLOBAL.SUCCES_MESSAGE,
            data: callbackData
        });
    }).catch(errCallback => {
        res.status(500).send({
            message: errCallback.message || GLOBAL.ERROR_SAVING
        });
    });
};


exports.findAll = (req, res) => {
    CatProductos.find()
    .populate({path:'categoria_id',select:'categoria'})
    .then(callbackUsuairo => {
        res.send(callbackUsuairo);
    }).catch(err => {
        res.status(500).send({
            message: err.message || GLOBAL.ERROR_RETRIEVING_DATA
        });
    });
};

exports.findOne = (req, res) => {
    var id = req.params.catProductoId;    
    CatProductos.findById({_id: id })
    .populate('categoria_id', 'categoria')
    .then(callbackResponse => {
        if(!callbackResponse) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });            
        }
        res.send(callbackResponse);
    }).catch(err => {
        if(err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_SEARCHING_DATA
        });
    });
};


exports.update = (req, res) => {
    if (!req.params.catProductoId) {
        return res.status(400).send({
            parametro:GLOBAL.PARAMETERS.PRODUCTO_ID,
            message: GLOBAL.MISSING_PARAMETER
        });
    }

    var id = req.params.catProductoId;    
    CatProductos.findByIdAndUpdate({ _id: id }, {
        producto: req.body.producto,
        categoria_id: req.body.categoria_id,
        precio: req.body.precio,
        cantidad: req.body.cantidad,
        activo: req.body.activo || false
    }, {new: true})
    .then(updateCallback => {
        if(!updateCallback) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });
        }
        res.send(updateCallback);
    }).catch(err => {
        if(err.kind === GLOBAL.ERROR_KIND.OBJECTID) {
            return res.status(404).send({
                message: GLOBAL.NOT_WELL_FORMED_ID
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_UPDATING
        });
    });
};

const validacionesBusqueda =(req)=>{
    data = {
        "message" : '',
        "status"  : true
    };

    let categoria_id = req.params.categoria_id;

    if(!req.params.categoria_id) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.parametro = GLOBAL.PARAMETERS.CATEGORIA_ID
        data.status = false;
        return data;
    }

    if(!mongoose.Types.ObjectId.isValid(categoria_id)) {
        data.message = GLOBAL.MISSING_PARAMETER
        data.parametro = GLOBAL.NOT_VALID_ID
        data.status = false;
        return data;
    }

    return data;
}

exports.findByCategoriaId = (req, res) =>{
    var validacion = validacionesBusqueda(req);
    if(!validacion.status) {
        return res.status(400).send(validacion.message);
    }
    let categoria_id=  req.params.categoria_id;
    //TODO: PRODUCTOS CON IMAGENES
    // getProductosByCategoriaId(categoria_id)
    getProductosYimagenesByCategoria(categoria_id)
    .then(success=>{
        res.send(success);
    }).catch(error=>{
        if(error.kind === GLOBAL.ERROR_KIND.OBJECTID){
            return res.status(404).send({ message: GLOBAL.NOT_WELL_FORMED_ID, error:error.message }); 
        }
        
        return res.status(500).send({  message: GLOBAL.ERROR_RETRIEVING_DATA, error:error.message });
    
    });
}

let getProductosYimagenesByCategoria =  async (categoria_id)=>{
    let productos = await getProductosByCategoriaId(categoria_id);
    let data = productos.map( async (item)=>{
        let data ={};
        data.categoria_id =item.categoria_id;
        data._id = item._id;
        data.producto =item.producto;
        data.precio =item.precio;
        data.cantidad =item.cantidad;
        data.activo =item.activo;
        data.createdAt = item.createdAt;
        data.updatedAt = item.updatedAt;
        let fileInfo = await getFilesByItemId(item._id).then((data)=> {return data}).catch(error=>{return error});
        data.img = fileInfo;
        return data;
    });

    let finalData = await Promise.all(data);
    return finalData;
}

let getProductosByCategoriaId = (categoria_id)=>{
    return new Promise((resolve, reject)=>{
        CatProductos.find({ categoria_id: categoria_id })
        .then(success=>{
            resolve(success);
        }).catch(error=>{
            reject(error);
        });
    });
}

let getFilesByItemId = (item_id) => {
    return new Promise((resolve, reject)=>{
        File.fileSchema.find({ item_id: item_id })
            .then(responseData => {
                resolve(responseData);
            }).catch(errorData => {
                reject(errorData);
            });
    });
}

exports.delete = (req, res) => {
    CatProductos.findByIdAndRemove(req.params.catProductoId)
    .then(catproducto => {
        if(!catproducto) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });
        }
        res.send({message: GLOBAL.SUCCES_DELETE});
    }).catch(err => {
        if(err.kind === GLOBAL.ERROR_KIND.OBJECTID || err.name === GLOBAL.ERROR_KIND.NOT_FOUND) {
            return res.status(404).send({
                message: GLOBAL.NOT_FOUND_ID
            });                
        }
        return res.status(500).send({
            message: GLOBAL.ERROR_DELETING
        });
    });
};

exports.tieneExtras = (req, res) =>{
    let id = req.params.item_id;
    extras(id)
    .then(success => res.send(success))
    .catch(error=> res.status(500).send({ error:error.message }) );
}

let extras = async(item_id)=>{
    let aderezo = await globalFunctions.relacion_item_aderezo(item_id);
    let proteina = await globalFunctions.relacion_item_proteinas(item_id);
    let sabores = await globalFunctions.relacion_item_sabores(item_id);
    let saboresdulces = await globalFunctions.relacion_item_sabores_dulces(item_id);
    let tipo = await globalFunctions.relacion_item_tipobebida(item_id);

    let data = {};
    data.valido = false
    
    if(aderezo.length > 0){
        data.valido = true;
        data.aderezo = true;
    }else{
        data.aderezo = false;
    }

    if(saboresdulces.length > 0){
        data.valido = true;
        data.saboresdulces = true;
    }else{
        data.saboresdulces = false; 
    }

    if(proteina.length > 0){
        data.valido = true;
        data.proteina =true;
    }else{
        data.proteina =false;
    }

    if(sabores.length > 0){
        data.valido = true;
        data.sabores =true;
    }else{
        data.sabores =false;
    }

    if(tipo.length > 0){
        data.valido = true;
        data.tipo =true;
    }else{
        data.tipo =false;
    }
    
    return data;
}