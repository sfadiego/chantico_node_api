const Rel_Producto_Aderezo = require('../model/productos/productoaderezos.model');
const Rel_Proteina_Aderezo = require('../model/productos/proteina.model');
const Rel_Productos_Sabores = require('../model/productos/sabores.model');
const Rel_Producto_TipoBebida = require('../model/productos/tipobebida.model');
const Pedidos = require('../model/pedidos.model');
const CatStatus = require('../model/catestatuspedido.model');
const Rel_Comanda_Pedido = require('../model/comandapedido.model');
const mongoose = require('mongoose');
const CONSTANTES = require('../controllers/globalconstantes')
exports.relacion_item_aderezo = (item_id) => {
    let id = item_id;
    return new Promise((resolve, reject) => {
        Rel_Producto_Aderezo.find({ producto_id: id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.relacion_item_proteinas = (item_id) => {
    let id = item_id;
    return new Promise((resolve, reject) => {
        Rel_Proteina_Aderezo.productoproteina.find({ producto_id: id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.relacion_item_sabores = (item_id) => {
    let id = item_id;
    return new Promise((resolve, reject) => {
        Rel_Productos_Sabores.productosabores.find({ producto_id: id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.relacion_item_sabores_dulces = (item_id) => {
    let id = item_id;
    return new Promise((resolve, reject) => {
        Rel_Productos_Sabores.productosaboresdulces.find({ producto_id: id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.relacion_item_tipobebida = (item_id) => {
    let id = item_id;
    return new Promise((resolve, reject) => {
        Rel_Producto_TipoBebida.productotipobebida.find({ producto_id: id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.crear_relacion_pedido_item = (pedido_id, obj_extra) => {
    return new Promise((resolve, reject) => {
        const PedidosExtras = new Pedidos.pedidoextras({
            pedido_id: pedido_id,
            nombre: obj_extra.nombre,
            precio: obj_extra.precio,
            unique: obj_extra.unique,
            key: obj_extra.key,
        });

        PedidosExtras
            .save()
            .then(responseData => resolve(responseData)).catch(error => reject(error));

    });
}


exports.relacion_pedido_item = (id) => {
    let pedido_id = id;
    return new Promise((resolve, reject) => {
        Pedidos.pedidoextras.find({ pedido_id: pedido_id })
            .then(responseData => resolve(responseData))
            .catch(errorData => reject(errorData));
    });
}

exports.save_relacion_comanda_pedido = (object, pedido_id) => {
    return new Promise(async (resolve, reject) => {
        let obj = new Rel_Comanda_Pedido({
            precio: object.precio,
            producto: object.producto,
            key: object.key,
            descuento: object.descuento || 0,
            pedido_id: pedido_id,
            cantidad: object.cantidad,
            unique: object.unique
        });
        obj.save()
            .then((result) => resolve(result))
            .catch((err) => reject(err));
    });
}

exports.findAndUpdatePedido = (pedido_id, object) => {
    let id = pedido_id;
    let obj = object;
    return new Promise((resolve, reject) => {
        Pedidos.pedido.findByIdAndUpdate({
            _id: id
        }, {
            total: obj.total,
            subtotal: obj.subtotal,
            descuento: obj.descuento || 0
        }).then(success => resolve(success))
            .catch(err => reject(err));

    })
};

exports.findOnePedido = (id) => {
    return new Promise((resolve, reject) => {
        Pedidos.pedido.findById({ _id: id })
            .populate('estatus_id')
            .then(response => resolve(response))
            .catch(err => reject(err));
    })
}

exports.getRelComandaByPedidoId = (pedido_id) => {
    return new Promise((resolve, reject) => {
        Rel_Comanda_Pedido.find({ 'pedido_id': pedido_id })
            .then(response => resolve(response))
            .catch(err => reject(err));
    })
}

exports.getDate = () => { return new Date(); }

exports.generaFolio = () => {
    var date = new Date();
    let key = date.getMilliseconds();
    return `CHANT-${key}-${date.getDay()}-${date.getMonth()}`
}

exports.getStatusOcupado = () => {
    return new Promise((resolve, reject) => {
        CatStatus.find({ estatus: "ocupado" })
            .then(response => resolve(response))
            .catch(err => reject(err));
    })
}

exports.getEstatusProceso = () => {
    return new Promise((resolve, reject) => {
        CatStatus.find({ estatus: "proceso" })
            .then(response => resolve(response))
            .catch(err => reject(err));
    })
}

exports.getEstatusFinalizado = () => {
    return new Promise((resolve, reject) => {
        CatStatus.find({ estatus: "finalizado" })
            .then(response => resolve(response))
            .catch(err => reject(err));
    })
}


exports.errorHandler = (error, status) => {
    switch (status) {
        case 400: return { msg: error.msg, ok: false }
        case 500: return { msg: error.message, error: error.errors, ok: false }
        case 401: return { msg: error.msg, error: error, ok: false }
        default: return { error };
    }

}

exports.getOpenSalesDate = () => {
    let fecha = new Date();
    return  new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate() + 1)
}