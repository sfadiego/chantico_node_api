// mongose
const mongoose = require('mongoose');
mongoose.set('debug', true);
mongoose.Promise = global.Promise;
mongoose.connect(process.env.URLDB, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => console.log("Conectado a la base de datos"))
    .catch(error => {
        console.log('No se puede conectar a la base de datos. Saliendo...', error);
        process.exit();
    });
