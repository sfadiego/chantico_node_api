const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const Schema = mongoose.Schema;
const GLOBAL = require('../controllers/globalconstantes');

const pedidosSchema = Schema({
    total:{
        type:Number,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TOTAL}`]
    },
    subtotal:{
        type:Number,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.SUBTOTAL}`]
    },
    descuento:Number,
    fecha_pedido:Date,
    nombre_pedido:String,
    estatus_id:[
        { 
            type: Schema.Types.ObjectId, ref: 'catEstatusPedido',
            required:true
        }
    ]
}, {
    timestamps: true
});

const pedidoExtrasSchema = Schema({
    pedido_id:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PEDIDO_ID}`],
    },
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.NOMBRE_PRODUCTO}`],
    },
    precio:{
        type:Number,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRECIO}`],
    },
    unique:{
        type:Number,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.UNIQUE}`],
    },
    key:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.KEY}`],
    }
    
}, {
    timestamps: true
});

module.exports ={
    pedido:mongoose.model('pedido', pedidosSchema),
    pedidoextras:mongoose.model('pedidoextras', pedidoExtrasSchema)   
}