const mongoose = require('mongoose');
const GLOBAL = require('../controllers/globalconstantes');
const Schema = mongoose.Schema;

const catestatusPedidoSchema = Schema({
    estatus: {
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.ESTATUS}`],
        createIndexes:true
    },
    icon:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.ICON}`],
        createIndexes:true
    },
    color:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.COLOR}`],
        createIndexes:true
    },
    cssclass:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.CSSCLASS}`],
        createIndexes:true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('catEstatusPedido', catestatusPedidoSchema);