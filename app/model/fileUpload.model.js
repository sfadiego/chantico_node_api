const mongoose = require('mongoose');
const GLOBAL = require('../controllers/globalconstantes');
const Schema = mongoose.Schema;

const fileSchema = mongoose.Schema({
    file_path: {
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.FILE_PATH}`]
    },
    folder:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.FOLDER}`]
    },
    fileName:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.FILE}`],
        createIndexes:true,
    },
    public_path: {
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PUBLIC_PATH}`]
    },
    item_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.SABOR}`]
    },
   
}, {
    timestamps: true
});

const itemsArchivosSchema = mongoose.Schema({
    item_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.SABOR}`]
    },
    archivo_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.ARCHIVO_ID}`],
        createIndexes:true
    }
    
},{
    timestamps: true
});


module.exports = {
    fileSchema:mongoose.model('fileUpload',fileSchema),
    itemsArchivos:mongoose.model('itemsArchivos',itemsArchivosSchema)

}