const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const comandaPedidoSchema = Schema({
    precio:Number,
    producto:String,
    unique:Number,
    descuento:Number,
    key:String,
    pedido_id:{
        type:String, 
        required:[true,'Parametro necesario']
    },
    cantidad:Number
}, {
    timestamps: true
});

module.exports = mongoose.model('comandapedido', comandaPedidoSchema);