const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GLOBAL = require('../../controllers/globalconstantes');

const productoaderezoSchema = mongoose.Schema({
    producto_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRODUCTO_ID}`],
    },
    aderezo_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.ADEREZO_ID}`],
    }
},
{
    timestamps: true
});

module.exports = mongoose.model('productoaderezos', productoaderezoSchema);

