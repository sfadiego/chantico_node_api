const mongoose = require('mongoose');
const GLOBAL = require('../../controllers/globalconstantes');
const Schema = mongoose.Schema;

const aderezoSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.ADEREZO}`],
        createIndexes:true
    },
    precio:{
        type:Number,
        default:0
    }
},{
    timestamps: true
});

module.exports = mongoose.model('aderezo', aderezoSchema);

