const mongoose = require('mongoose');
const GLOBAL = require('../../controllers/globalconstantes');
const Schema = mongoose.Schema;

const proteinaSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PROTEINA}`],
        createIndexes:true
    },
    precio:{
        type:Number,
        default:0
    }    
},{
    timestamps: true
});

const productoproteinaSchema = mongoose.Schema({
    producto_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.SABOR}`]
    },
    proteina_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PROTEINA_ID}`],
    }
    
},{
    timestamps: true
});


module.exports = {
    proteina:mongoose.model('proteina',proteinaSchema),
    productoproteina:mongoose.model('productoproteina',productoproteinaSchema)

}

