const mongoose = require('mongoose');
const GLOBAL = require('../../controllers/globalconstantes');
const Schema = mongoose.Schema;

const saboresSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.SABOR}`],
        createIndexes:true
    },
    precio:{
        type:Number,
        default:0
    }
},{
    timestamps: true
});

const productosaboresSchema = mongoose.Schema({
    producto_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRODUCTO_ID}`],
    },
    sabor_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.SABOR_ID}`],
    }
},{
    timestamps: true
});

const sabordulceSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.SABOR}`],
        createIndexes:true
    },
    precio:{
        type:Number,
        default:0
    }
});

const productosaboresDulcesSchema = mongoose.Schema({
    producto_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRODUCTO_ID}`],
    },
    sabor_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.SABOR_ID}`],
    }
},{
    timestamps: true
});

module.exports = {
    sabores: mongoose.model('sabores', saboresSchema),
    productosabores: mongoose.model('productosabores', productosaboresSchema),
    sabordulce: mongoose.model('sabordulce',sabordulceSchema),
    productosaboresdulces: mongoose.model('productosaboresdulces',productosaboresDulcesSchema)
}