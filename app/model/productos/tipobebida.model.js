const mongoose = require('mongoose');
const GLOBAL = require('../../controllers/globalconstantes');
const Schema = mongoose.Schema;

const tipobebidaSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIPO_BEBIDA}`],
        createIndexes:true
    },
    precio:{
        type:Number,
        default:0
    }
},{
    timestamps: true
});

const productoTipoBebidaSchema = mongoose.Schema({
    producto_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRODUCTO_ID}`],
    },
    tipo_bebida_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIPO_BEBIDA_ID}`],
    },
},{
    timestamps: true
});

const pedidoTipoBebidaSchema = mongoose.Schema({
    pedido_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PEDIDO_ID}`],
    },
    tipo_bebida_id:{
        type:Schema.Types.ObjectId,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.TIPO_BEBIDA_ID}`],
    },
},{
    timestamps: true
});

module.exports = {
    productotipobebida: mongoose.model('productotipobebida', productoTipoBebidaSchema),
    tipobebida: mongoose.model('tipobebida', tipobebidaSchema),
    pedidotipobebida: mongoose.model('pedidotipobebida', pedidoTipoBebidaSchema),
    
};

