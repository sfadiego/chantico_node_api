const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const detalleProductoSchema = Schema({
    descripcion:String,
    precio_proveedor:Number,
    inventario:String,
    existencia:Number,
    inventariado:Boolean,
    tipo_producto:String,
    producto_id: [{
        type: Schema.Types.ObjectId,
        ref: 'catProductos',
        createIndexes:true,
        required: [true, 'Campo obligatorio']
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('DetalleProducto', detalleProductoSchema);

