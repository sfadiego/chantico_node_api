const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GLOBAL = require('../controllers/globalconstantes');

const catProductosSchema = Schema({
    producto: {
        type: String,
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRODUCTO}`],
        createIndexes: true
    },
    precio: {
        type: Number,
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.PRECIO}`]
    },
    cantidad: {
        type: String,
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.CANTIDAD}`]
    },
    categoria_id: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Categorias',
            required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.CATEGORIA_ID}`]
        }
    ],
    activo: Boolean
}, {
    timestamps: true
});

module.exports = mongoose.model('catProductos', catProductosSchema);