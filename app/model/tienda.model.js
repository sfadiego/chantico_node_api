const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GLOBAL = require('../controllers/globalconstantes');
const uniqueValidator = require('mongoose-unique-validator');

const tiendainfoSchema = Schema({
    tienda: {
        type: String,
        unique: true
    },
    cajaCerrada: {
        type: Boolean,
        default: false
    },
    cerradaHasta:{
        type:Date
    }
}, {
    timestamps: true
});

tiendainfoSchema.plugin(uniqueValidator, { msg: '{PATH} ' + GLOBAL.DUPLICATED_STORE });
module.exports = mongoose.model('tiendaInfo', tiendainfoSchema);