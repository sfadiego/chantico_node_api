const mongoose = require('mongoose');
const GLOBAL = require('../controllers/globalconstantes');

const categoriasSchema = mongoose.Schema({
    categoria: {
        type:String,
        required:[true,`${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.CATEGORIA}`],
        createIndexes:true
    },
    activo: Boolean,
    
}, {
    timestamps: true
});

module.exports = mongoose.model('Categorias', categoriasSchema);