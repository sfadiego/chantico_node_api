const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const GLOBAL = require('../controllers/globalconstantes');
const uniqueValidator = require('mongoose-unique-validator');
let roles = {
    values: [GLOBAL.ADMIN_ROLE, GLOBAL.USER_ROLE],
    message: '{VALUE} ' + GLOBAL.INVALID_ROLE
};
const userSchema = mongoose.Schema({
    nombre: {
        type: String,
        require: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PARAMETERS.NOMBRE}`]
    },
    apellido: String,
    username: {
        type: String,
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.USERNAME}`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.PASSWORD}`],
    },
    telefono: String,
    role: {
        type: Number,
        default: GLOBAL.USER_ROLE,
    },
    estado: {
        type: Boolean,
        default: true
    },
    tienda: {
        type: Schema.Types.ObjectId,
        ref: 'tiendaInfo',
        required: [true, `${GLOBAL.MISSING_PARAMETER} ${GLOBAL.ID}`]
    }
}, {
    timestamps: true
});

userSchema.methods.toJSON = function () {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;
    return userObject;
}
userSchema.plugin(uniqueValidator, { msg: '{PATH} ' + GLOBAL.DUPLICATED_USERNAME });
module.exports = mongoose.model('Usuarios', userSchema);