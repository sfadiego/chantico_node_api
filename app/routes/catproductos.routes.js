module.exports = (app) => {
    const catproductos = require('../controllers/catproductos.controller.js');

    // Create a new producto
    app.post('/catproductos', catproductos.create);

    // Retrieve all catproductos
    app.get('/catproductos', catproductos.findAll);

    // Retrieve a single Note with catProducto
    app.get('/catproductos/:catProductoId', catproductos.findOne);
    
    //getProductos by id categoria
    app.get('/findByCategoriaId/:categoria_id', catproductos.findByCategoriaId);

    // Update a Note with catProductoId
    app.put('/catproductos/:catProductoId', catproductos.update);

    // Delete a Note with catProductoId
    app.delete('/catproductos/:catProductoId', catproductos.delete);

    app.get('/tieneextrasitem/:item_id',catproductos.tieneExtras)
}