module.exports = (app) => {
    const fileUpload = require('express-fileupload');
    const file = require('../controllers/fileUpload.controller.js');
    app.use(fileUpload());
    app.put('/uploadFile/:nombre_carpeta', file.uploadFile);

    app.get('/fileinfoByIdItem/:item_id', file.getFileByItemId);

}