module.exports = (app) => {
    const categoria = require('../controllers/categoria.controller.js');

    // Create a new Note
    app.post('/categoria', categoria.create);

    // Retrieve all categoria
    app.get('/categoria', categoria.findAll);

    // Retrieve a single Note with noteId
    app.get('/categoria/:categoriaId', categoria.findOne);

    // Update a Note with categoriaId
    app.put('/categoria/:categoriaId', categoria.update);

    // Delete a Note with categoriaId
    app.delete('/categoria/:categoriaId', categoria.delete);
}