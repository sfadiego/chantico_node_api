module.exports = (app) => {
    const usuario = require('../controllers/usuarios.controller.js');

    // Create a new Note
    app.post('/usuario', usuario.create);

    // Retrieve all usuario
    app.get('/usuario', usuario.findAll);

    // Retrieve a single Note with noteId
    app.get('/usuario/:usuarioId', usuario.findOne);

    // Update a Note with usuarioId
    app.put('/usuario/:usuarioId', usuario.update);

    // Delete a Note with usuarioId
    app.delete('/usuario/:usuarioId', usuario.delete);

    app.post('/cerrarcaja', usuario.cerrarCaja)
    app.post('/abrircaja', usuario.abrirCaja)
    
}