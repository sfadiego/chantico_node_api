module.exports = (app) => {
    const detalleProducto = require('../controllers/detalleproducto.controller.js');
    
    // Create a new Note
    app.post('/detalleProducto', detalleProducto.create);

    // Retrieve all detalleProducto
    app.get('/detalleProducto', detalleProducto.findAll);

    // Retrieve a single Note with noteId
    app.get('/detalleProducto/:detalle_producto_id', detalleProducto.findOne);

    // Update a Note with detalleProductoId
    app.put('/detalleProducto/:detalle_producto_id', detalleProducto.update);

    // Delete a Note with detalleProductoId
    app.delete('/detalleProducto/:detalle_producto_id', detalleProducto.delete);
    
}