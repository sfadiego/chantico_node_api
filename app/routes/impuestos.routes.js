module.exports = (app) => {
    const impuesto = require('../controllers/impuesto.controller.js');

    // Create a new Note
    app.post('/impuesto', impuesto.create);

    // Retrieve all impuesto
    app.get('/impuesto', impuesto.findAll);

    // Retrieve a single Note with noteId
    app.get('/impuesto/:impuestoId', impuesto.findOne);

    // Update a Note with impuestoId
    app.put('/impuesto/:impuestoId', impuesto.update);

    // Delete a Note with impuestoId
    app.delete('/impuesto/:impuestoId', impuesto.delete);
}