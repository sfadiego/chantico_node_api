module.exports = (app) => {
    const proteina = require('../../controllers/proteina.controller');
    
    app.post('/proteina', proteina.crear);
    app.post('/productos/relproteina', proteina.crearrelacion);

    app.get('/proteina', proteina.getAllTipoproteina);
    app.get('/proteina/:id', proteina.getOne);
    app.put('/proteina/:id', proteina.update);

    app.get('/productos/proteina/:producto_id', proteina.getrelacionproductosproteina);


    app.delete('/productos/relproteina/:id_relacion', proteina.deleterelacionproductosproteina);
}