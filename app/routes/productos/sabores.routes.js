module.exports = (app) => {
    const sabores = require('../../controllers/sabores.controller');
    
    app.post('/sabores', sabores.crear);
    app.post('/productos/relproductosabores', sabores.createrelproductosabores);

    app.get('/sabores', sabores.getAllSabores);
    app.get('/sabores/:id', sabores.getOne);
    app.put('/sabores/:id',sabores.update);

    app.get('/productos/getrelacionproductosaboresbyProducto/:producto_id', sabores.getrelacionproductossabores);
    app.delete('/productos/deleterelacionproductosabores/:id_relacion', sabores.deleterelacionproductos);

    app.get('/saboresdulces', sabores.getSaboresDulces);
    app.get('/saboresdulces/:id', sabores.getOneSaborDulce);
    app.post('/sabordulce', sabores.createSaborDulce);
    app.put('/sabordulce/:id', sabores.updateSaborDulce);

    app.get('/productos/getrelacionproductossaboresdulcesbyProducto/:producto_id', sabores.getrelacionproductossaboresdulces);
    app.delete('/productos/deleterelacionproductos/:id_relacion', sabores.deleterelacionproductosabordulce);
    app.post('/productos/relproductosaboresdulces', sabores.createrelproductosaboresdulces);
}