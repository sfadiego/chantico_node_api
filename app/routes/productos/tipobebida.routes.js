module.exports = (app) => {
    const tipobebida = require('../../controllers/tipobebida.controller');
    
    app.post('/tipobebida', tipobebida.crear);
    app.post('/productos/relproductotipobebida', tipobebida.createrelproductotipobebida);
    app.post('/pedido/bebida/', tipobebida.createrelpedidoBebida);
    
    app.get('/tipobebida', tipobebida.getAllTipoBebida);
    app.put('/tipobebida/:id',tipobebida.update);
    app.get('/tipobebida/:id', tipobebida.getOne);

    app.get('/productos/getrelacionproductotipobebidabyProducto/:producto_id', tipobebida.getrelacionproductostipobebida);
    
    app.get('/pedido/bebida/:pedido_id', tipobebida.relpedidoBebida);

    app.delete('/productos/tipobebida/:id_relacion', tipobebida.deleterelacionproductostipobebida);
}