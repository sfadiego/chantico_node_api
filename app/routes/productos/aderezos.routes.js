module.exports = (app) => {
    const aderezos = require('../../controllers/aderezos.controller');

    // crear a new Note
    app.post('/aderezos', aderezos.crear);
    app.post('/productos/createrelproductoaderezos', aderezos.createrelproductoaderezos);

    // Retrieve all aderezos
    app.get('/aderezos', aderezos.getAll);
    app.get('/productos/getrelacionproductosaderezos', aderezos.getrelacionproductosaderezos);

    // Retrieve a single Note con noteId
    app.get('/aderezos/:aderezo_id', aderezos.getOne);
    app.get('/productos/getrelacionproductosaderezos/:producto_id', aderezos.getOnerelation);
    // Update a Note con aderezos_id
    app.put('/aderezos/:aderezo_id', aderezos.update);

    // Borrar a registro cont aderezos_id
    app.delete('/productos/deleterelacionproductosaderezos/:id_relacion', aderezos.deleterelacionproductos);
}