module.exports = (app) => {
    const pedidos = require('../controllers/pedido.controller.js');

    // Create a new Note
    app.post('/pedidos', pedidos.create);
    app.post('/pedidos/extras', pedidos.createpedidoItems);
    
    app.get('/pedidos', pedidos.findAll);

    app.get('/pedidosActivos', pedidos.getComandasActivas);
    
    app.get('/pedidos/:pedidoId', pedidos.findOne);
    
    app.put('/pedidos/finalizar/:pedido_id', pedidos.finalizapedido);
    app.put('/pedidos/ocupado/:pedido_id', pedidos.pedidoOcupado);
    
    app.get('/pedidos-terminados/', pedidos.pedidoFinalizados);
    app.post('/pedidos-terminados-by-fecha/', pedidos.pedidoFinalizadosByFecha);
    
    app.get('/totalventadiaria',pedidos.totalVentas)
}