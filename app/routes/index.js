const express = require('express')
const app = express();

require('./catproductos.routes')(app);
require('./categorias.routes.js')(app);
require('./pedido.routes.js')(app);
require('./comandapedido.routes.js')(app);
require('./catestatuspedido.routes.js')(app);
require('./fileUpload.routes.js')(app);
require('./imagenes.routes.js')(app);
require('./productos/aderezos.routes.js')(app);
require('./productos/sabores.routes.js')(app);
require('./productos/tipobebida.routes')(app);
require('./productos/proteina.routes')(app);
require('./login.routes')(app);
require('./usuario.routes')(app);

module.exports = app;