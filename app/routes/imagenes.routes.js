module.exports = (app) => {
    const path = require('path');
    const File = require('../model/fileUpload.model');
    const fs = require('fs')

    app.get('/imagen/:folder/:file_name', (req, res) => {
        let file_Name = req.params.file_name;
        let folder = req.params.folder;
        let pathimg = `/uploads/${folder}/${file_Name}`;
        // if (fs.existsSync(pathimg)) {
            res.sendFile(pathimg,{root:'./'});
        // }else{
            // res.sendFile(noimgPath);
        // }        

    });

    app.get('/noimagen',(req, res)=>{
        let noimgPath = path.resolve(__dirname,'../../public/images/chantico_secundario.png');
        res.sendFile(noimgPath);
    });

    app.get('/getByIdProducto/:id', (req,res)=>{
        let productoid = req.params.id;
        File.fileSchema.find({ producto_id: productoid })
        .then(success => res.send(success))
        .catch(err=> res.send(err));
    })
}
