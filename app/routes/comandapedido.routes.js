module.exports = (app) => {
    const comandapedido = require('../controllers/comandapedido.controller.js');
    app.post('/comandapedido/multiple', comandapedido.createComanda);

    app.post('/resumen/ventas/', comandapedido.getVentasDeDia);

    app.get('/comandaByPedidoId/:pedidoId', comandapedido.getComandaByPedidoId);
    app.get('/comanda/extras/:id', comandapedido.getExtrasComandaByPedidoId);
}