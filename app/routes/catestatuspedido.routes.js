module.exports = (app) => {
    const catestatuspedido = require('../controllers/catestatuspedido.controller.js');
    const { verificaToken } = require('../../middlewares/authentication');

    // Create a new producto
    app.post('/catestatuspedido', catestatuspedido.create);

    // Retrieve all catestatuspedido
    //midleware para verificar token
    app.get('/catestatuspedido', catestatuspedido.findAll);

    // Retrieve a single Note with catProducto
    app.get('/catestatuspedido/:estatusId', catestatuspedido.findOne);

    // Update a Note with estatusId
    app.put('/catestatuspedido/:estatusId', catestatuspedido.update);

    // Delete a Note with estatusId
    app.delete('/catestatuspedido/:estatusId', catestatuspedido.delete);
}