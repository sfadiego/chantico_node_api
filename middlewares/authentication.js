const jwt = require('jsonwebtoken');
const GLOBAL = require('../app/controllers/globalconstantes');
const functions = require('../app/core/genericfunctions.controller');

// verificar token
let verificaToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            let error = {
                msg: GLOBAL.INVALID_TOKEN,
                error: err
            }
            return res.status(401).json(functions.errorHandler(error, 401));
        }

        req.decodedtoken = decoded;

    });
    next();
}

module.exports = {
    verificaToken
};